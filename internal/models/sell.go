package models

import (
	"github.com/google/uuid"
	"time"
)

// данные использования распродажного товара в заказе
type Sell struct {
	SaleProductID uint
	OrderUUID     uuid.UUID // товарная часть заказа
	Quantity      uint      // сколько использовано
	CreatedAt     time.Time
}
