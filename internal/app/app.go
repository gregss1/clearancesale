package app

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"github.com/streadway/amqp"
	"sale/internal/app/calculator"
	"sale/internal/config"
	"sale/internal/models"
	"sale/internal/queue/producer"
	"sale/internal/storage"
	"time"
)

type CreateMessage struct {
	SaleID     uint
	RegionsIDs []uint
	Params     Params // todo по ссылке
}

type Params struct {
	NomenclatureUUID uuid.UUID
	Price            uint
	MaxCount         uint
	MaxOrderCount    uint
	Type             uint
	IsFeed           bool
}

type App struct {
	storage storage.Storage
	queue   amqp.Queue
	config  config.AppCfg
}

func NewApp(cfg config.AppCfg, stg storage.Storage) *App {
	return &App{storage: stg, config: cfg}
}

// CreateSale создать распродажу
func (a *App) CreateSale(sale *models.Sale) error {
	// todo валидация, проверка на пересечение с распродажей того же типа, что-то еще

	// сохранение в бд
	sale.UUID = uuid.New()
	sale.CreatedAt = time.Now()
	sale.CreatedBy = uuid.New()
	sale.State = models.SaleIsInactive // по дефолту неактивированная
	err := a.storage.CreateSale(sale)
	if err != nil {
		return err
	}

	// выгрузка во внешние системы (kafka)

	return nil
}

// AddToFillProductsByRegions добавляем в очередь
func (a *App) AddToFillProductsByRegions(saleID uint, regionIDs []uint, params []*Params) {
	// заполняем товары выбронной распродажи в выбранных регионах
	for _, p := range params {
		message := CreateMessage{
			SaleID:     saleID,
			RegionsIDs: regionIDs,
			Params:     *p,
		}

		b, _ := json.Marshal(&message)

		if err := producer.Publish(a.config.RabbitUri, string(b)); err != nil {
			log.Err(err).Msg("publish to rabbitMQ error")
		}
	}
}

// FillProductsByRegions непосредственно заполняем
// todo в приложении и продюсеры и консьюмеры, возможно нужно разнести
func (a *App) FillProductsByRegions(message CreateMessage) {
	// todo, без обновления, пока считаем что загружаем всегда новые данные
	for _, regionID := range message.RegionsIDs {
		if message.Params.Price == 0 {
			continue
		}

		saleProduct := &models.SaleProduct{
			UUID:             uuid.New(),
			NomenclatureUUID: message.Params.NomenclatureUUID,
			SaleID:           message.SaleID,
			RegionID:         regionID,
			MaxCount:         message.Params.MaxCount,
			MaxOrderCount:    message.Params.MaxOrderCount,
			Price:            message.Params.Price,
			IsFeed:           message.Params.IsFeed,
			StockAvailable:   0,                            //дефолтное значение
			Available:        0,                            //дефолтное значение
			Status:           models.SaleProductIsInactive, //дефолтное значение
			CreatedAt:        time.Now(),
			UpdatedAt:        time.Now(),
		}

		//добавляем деактивированную запись в бд
		err := a.storage.AddProduct(saleProduct)
		if err != nil {
			log.Err(err).Msg("error add product")
		}

		if err != nil {
			// todo добавить контекст в логи, regionID saleID..
			log.Err(err).Msg("err add init sale product")
			continue
		}

		err = a.calculateValues(saleProduct)
		if err != nil {
			log.Err(err).Msg("error calculate values")
			continue
		}
	}
}

// calculateValues расчитываем вычисляемые значения и обновляем в бд
func (a *App) calculateValues(saleProduct *models.SaleProduct) error {
	//кидаем ее на проверку лимитов и расчет остатков
	calc, err := calculator.NewCalculator(saleProduct, a.storage)
	if err != nil {
		return err
	}

	calcValues, err := calc.Calculate(saleProduct)
	if err != nil {
		return err
	}

	if calcValues == nil {
		return nil
	}

	err = a.storage.SaveParams(calcValues)
	if err != nil {
		return err
	}

	return nil
}

func (a *App) Sell(nomenclUUID uuid.UUID, quantity uint, linkItemUUID uuid.UUID) error {
	// todo ищем запись saleProduct для данного запроса
	// todo тут будет логика приоритета распродажи, проверка на лимиты через калькулятор
	saleProductID := map[uuid.UUID]uint{
		nomenclUUID: uint(1),
	}[nomenclUUID]

	sell := &models.Sell{
		SaleProductID: saleProductID,
		OrderUUID:     linkItemUUID,
		Quantity:      quantity,
	}
	err := a.storage.AddSell(sell)
	if err != nil {
		return err
	}
	return nil
}
