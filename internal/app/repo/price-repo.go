package repo

import (
	"sale/internal/app/client"

	"github.com/google/uuid"
)

type PriceGetter interface {
	GetSalePrice(regionID int, nomenclUUID uuid.UUID) uint
	GetPurchasePrice(nomenclUUID uuid.UUID) uint
}

type PriceRepo struct {
	client client.ScroogeClient
}

func (r *PriceRepo) GetSalePrice(regionID uint, nomenclUUID uuid.UUID) uint {
	return r.client.GetSalePrice(regionID, nomenclUUID)
}

func (r *PriceRepo) GetPurchasePrice(nomenclUUID uuid.UUID) uint {
	return r.client.GetPurchasePrice(nomenclUUID)
}
