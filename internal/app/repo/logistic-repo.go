package repo

import (
	"sale/internal/app/client"

	"github.com/google/uuid"
)

type LogisticRepo interface {
	GetLogisticOfficesTo(officesToUUID uuid.UUID) []uuid.UUID
}

type SusaninRepo struct {
	client client.SusaninClient
}

func (r *SusaninRepo) GetLogisticOfficesTo(officesToUUID uuid.UUID) []uuid.UUID {
	return r.client.GetLogisticOfficesTo(officesToUUID)
}
