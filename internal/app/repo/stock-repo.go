package repo

import (
	"sale/internal/app/client"

	"github.com/google/uuid"
)

type StockRepo interface {
	GetRestsByOffices(officesUUIDs []uuid.UUID, nomenclUUID uuid.UUID) int
}

type ShcatRepo struct {
	client client.ShcatClient
}

func (r *ShcatRepo) GetRestsByOffices(officesUUIDs []uuid.UUID, nomenclUUID uuid.UUID) int {
	return r.client.GetRestsByOffices(officesUUIDs, nomenclUUID)
}
