package repo

import (
	"sale/internal/app/client"

	"github.com/google/uuid"
)

type ProviderRepo interface {
	HasProvider(nomenclUUID uuid.UUID) bool
}

type PDMRepo struct {
	client client.PdmClient
}

func (r *PDMRepo) HasProvider(nomenclUUID uuid.UUID) bool {
	return r.client.HasProvider(nomenclUUID)
}
