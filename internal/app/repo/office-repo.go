package repo

import (
	"sale/internal/app/client"

	"github.com/google/uuid"
)

type OfficeRepo interface {
	GetOfficesUUIDsByRegion(regionID uint) []uuid.UUID
	GetMainStoreOfficeInRegion(regionID uint) uuid.UUID
}

type RORepo struct { // мс регионов и офисов РиО
	client client.OfficerClient
}

func (r *RORepo) GetOfficesUUIDsByRegion(regionID uint) []uuid.UUID {
	return r.client.GetOfficesUUIDsByRegion(regionID)
}

func (r *RORepo) GetMainStoreOfficeInRegion(regionID uint) uuid.UUID {
	return r.client.GetMainStoreOfficeInRegion(regionID)
}
