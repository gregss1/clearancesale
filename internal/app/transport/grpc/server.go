package server

import (
	"context"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/protobuf/types/known/emptypb"
	salev1 "sale/gen/api/public/v1"
	saleuiv1 "sale/gen/api/ui/v1"
	"sale/internal/app"
	"sale/internal/models"
	"sale/internal/pkg/excel"
)

type Server struct {
	saleApp *app.App
}

func NewServer(saleApp *app.App) *Server {
	return &Server{saleApp: saleApp}
}

func (s *Server) CreateSale(ctx context.Context, r *saleuiv1.CreateSaleRequest) (*emptypb.Empty, error) {
	createSale := r.CreateSale
	// todo проверка/валидация?
	sale := &models.Sale{
		Name:           createSale.Name,
		StartDate:      createSale.StartDate.AsTime(),
		EndDate:        createSale.EndDate.AsTime(),
		StopFactor:     uint8(createSale.StopFactor),
		ContractorType: uint8(createSale.ContractorType),
		Type:           uint8(createSale.Type),
	}

	err := s.saleApp.CreateSale(sale)
	if err != nil {
		log.Err(err).Msg("error create sale")
	}

	return &emptypb.Empty{}, nil
}

func (s *Server) ImportSaleProducts(ctx context.Context, r *saleuiv1.ImportSaleProductsRequest) (*emptypb.Empty, error) {
	importProducts := r.ImportSaleProducts

	params, err := excel.ReadByte(importProducts.Params)
	if err != nil {
		log.Err(err).Msg("error read excel file")
	}

	s.saleApp.AddToFillProductsByRegions(uint(importProducts.SaleID), []uint{uint(importProducts.RegionID)}, params)

	return &emptypb.Empty{}, nil
}

func (s *Server) SubmitSelling(ctx context.Context, r *salev1.SubmitSellingRequest) (*salev1.SubmitSellingResponse, error) {
	selling := r.GetSelling()
	// todo валидация
	nomenclUUID, err := uuid.Parse(selling.NomenclatureUuid)
	if err != nil {
		return nil, err
	}
	linkItemUUID, err := uuid.Parse(selling.OrderUuid)
	if err != nil {
		return nil, err
	}

	// todo проверка что действие допустимо
	// canSelling(selling.Quantity, selling.OrderUuid, selling.NomenclatureUuid)

	err = s.saleApp.Sell(nomenclUUID, uint(selling.Quantity), linkItemUUID)
	if err != nil {
		return nil, err
	}

	return &salev1.SubmitSellingResponse{}, nil
}
