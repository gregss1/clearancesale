package calculator

import (
	"github.com/stretchr/testify/require"
	"testing"
)

type officeRepoMock struct{}

//todo заменить на golang/mock или подобный
func (c *officeRepoMock) GetOfficesUUIDsByRegion(regionID int) []string {
	return []string{"officeUUID1", "officeUUID2"}
}
func (c *officeRepoMock) GetMainStoreOfficeInRegion(regionID int) string {
	return "officeUUID3"
}

type stockRepoMock struct{}

func (c *stockRepoMock) GetRestsByOffices(officesUUIDs []string, nomenclUUID string) int {
	var stockData = map[string]map[string]int{
		"officeUUID1": {"nomenclUUID1": 1},
		"officeUUID2": {"nomenclUUID1": 2, "nomenclUUID2": 1},
		"officeUUID3": {"nomenclUUID1": 1},
		"officeUUID4": {"nomenclUUID1": 1},
		"officeUUID5": {"nomenclUUID1": 1},
	}

	rest := 0
	for _, officeUUID := range officesUUIDs {
		if r, ok := stockData[officeUUID][nomenclUUID]; ok {
			rest += r
		}
	}

	return rest
}

type logisticRepoMock struct{}

func (c *logisticRepoMock) GetLogisticOfficesTo(officesToUUID string) []string {
	var logisticData = map[string][]string{
		"officeUUID3": {"officeUUID4", "officeUUID5"},
		"officeUUID4": {"officeUUID5"},
	}

	if r, ok := logisticData[officesToUUID]; ok {
		return r
	}

	return nil
}

type providerRepoMock struct{}

func (c *providerRepoMock) HasProvider(nomenclUUID string) bool {
	var providerData = map[string]bool{
		"nomenclUUID1": true,
		"nomenclUUID2": false,
	}

	if r, ok := providerData[nomenclUUID]; ok {
		return r
	}

	return false
}

func TestInRegionStopFactorCalculator(t *testing.T) {
	calculator := &InRegionStopFactorCalculator{officeRepo: &officeRepoMock{}, stockRepo: &stockRepoMock{}}

	require.Equal(t, 3, calculator.Calculate("nomenclUUID1", 1))
	require.Equal(t, 1, calculator.Calculate("nomenclUUID2", 1))
}

func TestMainDistributeCenterStopFactorCalculator(t *testing.T) {
	calculator := &MainDistributeCenterStopFactorCalculator{
		officeRepo:   &officeRepoMock{},
		stockRepo:    &stockRepoMock{},
		logisticRepo: &logisticRepoMock{},
	}

	require.Equal(t, 5, calculator.Calculate("nomenclUUID1", 1))
	require.Equal(t, 1, calculator.Calculate("nomenclUUID2", 1))
}

func TestLogisticChainStopFactorCalculator(t *testing.T) {
	calculator := &LogisticChainStopFactorCalculator{
		providerRepo: &providerRepoMock{},
		sfcalc: &MainDistributeCenterStopFactorCalculator{
			officeRepo:   &officeRepoMock{},
			stockRepo:    &stockRepoMock{},
			logisticRepo: &logisticRepoMock{},
		},
	}

	require.Equal(t, 9999999, calculator.Calculate("nomenclUUID1", 1))
	require.Equal(t, 1, calculator.Calculate("nomenclUUID2", 1))
}
