package calculator

import (
	"sale/internal/models"
	"sale/internal/pkg/math"
)

type LimitApplier interface {
	ApplyLimits(product *models.SaleProduct, stockAvailable int) int
}

type PurchasedCountGetterInterface interface {
	GetPurchasedCount(product *models.SaleProduct) (int, error)
}

type LimitCalculator struct {
	storage PurchasedCountGetterInterface
}

func (c *LimitCalculator) ApplyLimits(product *models.SaleProduct, stockAvailable int) int {
	available := stockAvailable
	if product.MaxCount > 0 {
		maxCountLimitAvailable := c.maxCountLimit(product)
		available = math.Min(available, maxCountLimitAvailable)
	}

	if product.MaxOrderCount > 0 {
		available = math.Min(available, int(product.MaxOrderCount))
	}

	return available
}

func (c *LimitCalculator) maxCountLimit(product *models.SaleProduct) int {
	purchasedCount, err := c.storage.GetPurchasedCount(product)
	if err != nil {
		//log(err.Error())
		return 0
	}

	if purchasedCount > int(product.MaxCount) {
		return 0
	}

	return int(product.MaxCount) - purchasedCount
}
