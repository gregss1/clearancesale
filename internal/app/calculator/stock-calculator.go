package calculator

import (
	"errors"
	"github.com/google/uuid"
	"sale/internal/app/repo"
	"sale/internal/models"
)

type StockCalculator interface {
	Calculate(nomenclUUID uuid.UUID, regionID uint) int
}

type StockAvailableCalculator struct {
	stopFactorCalculator StopFactorCalculator
}

func (c *StockAvailableCalculator) Calculate(nomenclUUID uuid.UUID, regionID uint) int {
	return c.stopFactorCalculator.Calculate(nomenclUUID, regionID)
}

func NewStockAvailableCalculator(stopFactor uint) (*StockAvailableCalculator, error) {
	var stopFactorCalculator StopFactorCalculator

	switch stopFactor {
	case models.InRegionStopFactorID:
		stopFactorCalculator = &InRegionStopFactorCalculator{
			officeRepo: &repo.RORepo{},
			stockRepo:  &repo.ShcatRepo{},
		}
	case models.WithoutMainDistributeCenterStopFactorID:
		stopFactorCalculator = &WithoutMainDistributeCenterStopFactorCalculator{
			officeRepo:   &repo.RORepo{},
			stockRepo:    &repo.ShcatRepo{},
			logisticRepo: &repo.SusaninRepo{},
		}
	case models.MainDistributeCenterStopFactorID:
		stopFactorCalculator = &MainDistributeCenterStopFactorCalculator{
			officeRepo:   &repo.RORepo{},
			stockRepo:    &repo.ShcatRepo{},
			logisticRepo: &repo.SusaninRepo{},
		}
	case models.LogisticChainStopFactorID:
		stopFactorCalculator = &LogisticChainStopFactorCalculator{
			providerRepo: &repo.PDMRepo{},
			sfcalc: &MainDistributeCenterStopFactorCalculator{
				officeRepo:   &repo.RORepo{},
				stockRepo:    &repo.ShcatRepo{},
				logisticRepo: &repo.SusaninRepo{},
			},
		}
	default:
		return nil, errors.New("неизвестный стопфактор")
	}

	return &StockAvailableCalculator{stopFactorCalculator: stopFactorCalculator}, nil
}

// todo возможно в отедльный файл в том же namespace
const unlimit = 9999999

type StopFactorCalculator interface {
	Calculate(nomenclUUID uuid.UUID, regionID uint) int
}

type InRegionStopFactorCalculator struct {
	officeRepo repo.OfficeRepo
	stockRepo  repo.StockRepo
}
type WithoutMainDistributeCenterStopFactorCalculator struct {
	officeRepo   repo.OfficeRepo
	stockRepo    repo.StockRepo
	logisticRepo repo.LogisticRepo
}
type MainDistributeCenterStopFactorCalculator struct {
	officeRepo   repo.OfficeRepo
	stockRepo    repo.StockRepo
	logisticRepo repo.LogisticRepo
}
type LogisticChainStopFactorCalculator struct {
	providerRepo repo.ProviderRepo
	sfcalc       StopFactorCalculator
}

func (c *InRegionStopFactorCalculator) Calculate(nomenclUUID uuid.UUID, regionID uint) int {
	officesUUIDs := c.officeRepo.GetOfficesUUIDsByRegion(regionID)

	if len(officesUUIDs) == 0 {
		return 0
	}

	return c.stockRepo.GetRestsByOffices(officesUUIDs, nomenclUUID)
}

func (c *WithoutMainDistributeCenterStopFactorCalculator) Calculate(nomenclUUID uuid.UUID, regionID uint) int {
	//todo
	return 0
}

func (c *MainDistributeCenterStopFactorCalculator) Calculate(nomenclUUID uuid.UUID, regionID uint) int {
	officesUUIDs := append(
		c.logisticRepo.GetLogisticOfficesTo(c.officeRepo.GetMainStoreOfficeInRegion(regionID)),
		c.officeRepo.GetOfficesUUIDsByRegion(regionID)...,
	)

	if len(officesUUIDs) == 0 {
		return 0
	}

	return c.stockRepo.GetRestsByOffices(officesUUIDs, nomenclUUID)
}

func (c *LogisticChainStopFactorCalculator) Calculate(nomenclUUID uuid.UUID, regionID uint) int {
	if c.providerRepo.HasProvider(nomenclUUID) {
		return unlimit
	}

	//если поставщика нет, считаем остатки по лог. цепочке
	return c.sfcalc.Calculate(nomenclUUID, regionID)
}
