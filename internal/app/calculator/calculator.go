package calculator

import (
	"errors"
	"github.com/google/uuid"
	"math"
	"sale/internal/app/repo"
	"sale/internal/models"
)

const (
	maxRegionSalePriceRate = 0.99 //не более 0.99 от продажной цены в регионе
	minPurchasePriceRate   = 0.5  //не менее половины закупочной цены
)

// DTO изменяемых данных
// todo возможно это должна быть отдельная табличка в бд и вынести в другое место
type CalcValues struct {
	StockAvailable int
	Available      int
	Status         bool
}

type ParamsSaver interface {
	SaveParams(v *CalcValues) error
}

type Calculator struct {
	storage         ParamsSaver
	stockCalculator StockCalculator
	limitApplier    LimitApplier
	priceRepo       repo.PriceRepo
}

func NewCalculator(product *models.SaleProduct, storage ParamsSaver) (*Calculator, error) {
	stockAvailableCalculator, err := NewStockAvailableCalculator(product.SaleID)
	if err != nil {
		return nil, err
	}

	return &Calculator{
		storage:         storage,
		stockCalculator: stockAvailableCalculator,
		limitApplier:    &LimitCalculator{},
	}, nil
}

func (c *Calculator) Calculate(product *models.SaleProduct) (*CalcValues, error) {
	//выход из распродажи необратим
	if product.Status == models.SaleProductIsInactive {
		return nil, nil
	}

	if err := c.checkPrice(product); err != nil {
		return nil, err
	}

	stockAvailable := c.stockCalculator.Calculate(product.NomenclatureUUID, product.RegionID)
	available := c.limitApplier.ApplyLimits(product, stockAvailable)
	status := available > 0

	return &CalcValues{
		StockAvailable: stockAvailable,
		Available:      available,
		Status:         status,
	}, nil
}

func (c *Calculator) checkPrice(product *models.SaleProduct) error {
	if product.Price == 0.00 {
		return errors.New("нулевая цена")
	}

	maxRegionSalePrice := c.getMaxRegionSalePrice(product.RegionID, product.NomenclatureUUID)
	if maxRegionSalePrice == 0 {
		return errors.New("нулевая продажная цена в регионе")
	}
	if product.Price > maxRegionSalePrice {
		return errors.New("цена выше чем максимально возможная")
	}

	minPurchasePrice := c.getMinPurchasePriceRate(product.NomenclatureUUID)
	if minPurchasePrice == 0.00 {
		return errors.New("нулевая закупочная цена")
	}
	if product.Price < minPurchasePrice {
		return errors.New("цена ниже чем минимально возможная")
	}

	return nil
}

func (c *Calculator) getMaxRegionSalePrice(regionID uint, nomenclUUID uuid.UUID) uint {
	return uint(math.Round(float64(c.priceRepo.GetSalePrice(regionID, nomenclUUID)) * maxRegionSalePriceRate))
}

func (c *Calculator) getMinPurchasePriceRate(nomenclUUID uuid.UUID) uint {
	return uint(math.Round(float64(c.priceRepo.GetPurchasePrice(nomenclUUID)) * minPurchasePriceRate))
}
