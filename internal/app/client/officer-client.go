package client

import (
	"net/http"

	"github.com/google/uuid"
)

//todo заменить на sdk мс
const (
	getOfficesUUIDsByRegion = "/api/v1/json/getOfficesUUIDsByRegion"
)

type OfficerClient struct {
	client http.Client
}

// запрос к стороннему мс офисов на получение uuids офисов в регионе
func (c *OfficerClient) GetOfficesUUIDsByRegion(regionID uint) []uuid.UUID {
	_ = regionID
	_, _ = c.client.Get(getOfficesUUIDsByRegion)
	return nil
}

func (c *OfficerClient) GetMainStoreOfficeInRegion(regionID uint) uuid.UUID {
	_ = regionID
	_, _ = c.client.Get(getOfficesUUIDsByRegion)
	return uuid.Nil
}
