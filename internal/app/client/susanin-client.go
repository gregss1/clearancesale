package client

import (
	"net/http"

	"github.com/google/uuid"
)

//todo заменить на sdk мс
const (
	getLogisticOfficesTo = "/api/v1/json/getLogisticOfficesTo"
)

type SusaninClient struct { // Shcat - мс остатков
	client http.Client
}

func (c *SusaninClient) GetLogisticOfficesTo(officesToUUID uuid.UUID) []uuid.UUID {
	_ = officesToUUID
	_, _ = c.client.Get(getLogisticOfficesTo)
	return nil
}
