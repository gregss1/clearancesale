package client

import (
	"net/http"

	"github.com/google/uuid"
)

//todo заменить на sdk мс
const (
	getMaxRegionSalePrice   = "/api/v1/json/getMaxRegionSalePrice"
	getMinPurchasePriceRate = "/api/v1/json/getMinPurchasePriceRate"
)

type ScroogeClient struct {
	client http.Client
}

func (c *ScroogeClient) GetSalePrice(regionID uint, nomenclUUID uuid.UUID) uint {
	_ = regionID
	_ = nomenclUUID
	_, _ = c.client.Get(getMaxRegionSalePrice)
	return 300
}

func (c *ScroogeClient) GetPurchasePrice(nomenclUUID uuid.UUID) uint {
	_ = nomenclUUID
	_, _ = c.client.Get(getMinPurchasePriceRate)
	return 200
}
