package client

import (
	"net/http"

	"github.com/google/uuid"
)

//todo заменить на sdk мс
const (
	getRestsByOffices = "/api/v1/json/getRestsByOffices"
)

type ShcatClient struct { // Shcat - мс остатков
	client http.Client
}

func (c *ShcatClient) GetRestsByOffices(officesUUIDs []uuid.UUID, nomenclUUID uuid.UUID) int {
	_ = officesUUIDs
	_ = nomenclUUID
	_, _ = c.client.Get(getRestsByOffices)
	return 0
}
