package main

import (
	"context"
	"github.com/google/uuid"
	"sale/internal/app"
	"sale/internal/config"
	"sale/internal/storage/sql"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

const tsLayout = "2006-01-02T15:04:05"

// приложение для тестов в cli
func main() {
	ctx := context.TODO()

	cfg := &config.AppCfg{}
	config.LoadConfig(cfg)

	// todo вынести в утилиты
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	if cfg.IsDev {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	/*saleID := *pflag.Uint("saleID", 0, "")
	regionID := *pflag.Uint("regionID", 0, "")
	file := pflag.String("file", "", "")
	pflag.Parse()*/

	stg, err := sql.New(ctx, cfg.DbUri)
	if err != nil {
		log.Err(err).Msg("error new storage")
	}

	appSale := app.NewApp(*cfg, stg)
	/*startDate, _ := time.Parse(tsLayout, "2020-04-20T00:00:00")
	endDate, _ := time.Parse(tsLayout, "2020-04-21T00:00:00")
	sale := &models.Sale{
		Name:           "sale1",
		StartDate:      startDate,
		EndDate:        endDate,
		StopFactor:     1,
		ContractorType: 2,
		Type:           3,
	}

	err = appSale.CreateSale(sale)
	if err != nil {
		log.Err(err).Msg("error create sale")
	}*/

	/*if *file == "" {
		fmt.Println("empty file")
		return
	}

	fmt.Println(*file)*/

	/*params, err := excel.ReadFile("/home/gregs/GolandProjects/sale/docs/sales1.xlsx")
	if err != nil {
		fmt.Println(err.Error())
	}

	appSale.AddToFillProductsByRegions(1, []uint{1}, params)*/

	err = appSale.Sell(uuid.New(), uint(10), uuid.New())

}
