package main

import (
	"context"
	"net/http"
	saleuiv1 "sale/gen/api/ui/v1"
	"sale/internal/app"
	"sale/internal/app/transport/grpc"
	"sale/internal/config"
	"sale/internal/storage/sql"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func main() {
	ctx := context.TODO()
	cfg := &config.AppCfg{}
	config.LoadConfig(cfg)

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	if cfg.IsDev {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	stg, err := sql.New(ctx, cfg.DbUri)
	if err != nil {
		log.Err(err).Msg("error new storage")
	}

	handler := saleuiv1.NewSaleServiceServer(server.NewServer(app.NewApp(*cfg, stg)))
	mux := http.NewServeMux()
	mux.Handle(handler.PathPrefix(), handler)
	err = http.ListenAndServe(":8080", handler)
	if err != nil {
		log.Err(err).Msg("error listen and serve rpc server")
	}
}
