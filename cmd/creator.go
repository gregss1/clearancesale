package main

import (
	"context"
	"sale/internal/app"
	"sale/internal/app/creator"
	"sale/internal/config"
	"sale/internal/queue/consumer"
	"sale/internal/storage/sql"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// консьюмер, добавляющий позиции в распродажу и делающий расчет остатков
func main() {
	ctx := context.TODO()
	cfg := &config.AppCfg{}
	config.LoadConfig(cfg)

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	if cfg.IsDev {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	stg, err := sql.New(ctx, cfg.DbUri)
	if err != nil {
		log.Err(err).Msg("error new storage")
	}

	appSale := app.NewApp(*cfg, stg)

	c, err := consumer.New(cfg.RabbitUri, (&creator.CreateHandler{App: *appSale}).Handle)
	if err != nil {
		log.Err(err).Msg("error consume")
	}

	time.Sleep(time.Second * 60)

	log.Printf("shutting down")
	if err := c.Shutdown(); err != nil {
		log.Err(err).Msg("error during shutdown")
	}
}
