// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        (unknown)
// source: api/ui/v1/service.proto

package saleuiv1

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type TestRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Msg string `protobuf:"bytes,1,opt,name=msg,proto3" json:"msg,omitempty"`
}

func (x *TestRequest) Reset() {
	*x = TestRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_ui_v1_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TestRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TestRequest) ProtoMessage() {}

func (x *TestRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_ui_v1_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TestRequest.ProtoReflect.Descriptor instead.
func (*TestRequest) Descriptor() ([]byte, []int) {
	return file_api_ui_v1_service_proto_rawDescGZIP(), []int{0}
}

func (x *TestRequest) GetMsg() string {
	if x != nil {
		return x.Msg
	}
	return ""
}

// создать распродажу
type CreateSaleRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	CreateSale *CreateSaleRequest_CreateSale `protobuf:"bytes,1,opt,name=createSale,proto3" json:"createSale,omitempty"`
}

func (x *CreateSaleRequest) Reset() {
	*x = CreateSaleRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_ui_v1_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateSaleRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateSaleRequest) ProtoMessage() {}

func (x *CreateSaleRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_ui_v1_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateSaleRequest.ProtoReflect.Descriptor instead.
func (*CreateSaleRequest) Descriptor() ([]byte, []int) {
	return file_api_ui_v1_service_proto_rawDescGZIP(), []int{1}
}

func (x *CreateSaleRequest) GetCreateSale() *CreateSaleRequest_CreateSale {
	if x != nil {
		return x.CreateSale
	}
	return nil
}

// заполнить распродажу
type ImportSaleProductsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ImportSaleProducts *ImportSaleProductsRequest_ImportSaleProducts `protobuf:"bytes,1,opt,name=importSaleProducts,proto3" json:"importSaleProducts,omitempty"`
}

func (x *ImportSaleProductsRequest) Reset() {
	*x = ImportSaleProductsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_ui_v1_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ImportSaleProductsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ImportSaleProductsRequest) ProtoMessage() {}

func (x *ImportSaleProductsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_ui_v1_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ImportSaleProductsRequest.ProtoReflect.Descriptor instead.
func (*ImportSaleProductsRequest) Descriptor() ([]byte, []int) {
	return file_api_ui_v1_service_proto_rawDescGZIP(), []int{2}
}

func (x *ImportSaleProductsRequest) GetImportSaleProducts() *ImportSaleProductsRequest_ImportSaleProducts {
	if x != nil {
		return x.ImportSaleProducts
	}
	return nil
}

type CreateSaleRequest_CreateSale struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name           string                 `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	StartDate      *timestamppb.Timestamp `protobuf:"bytes,2,opt,name=start_date,json=startDate,proto3" json:"start_date,omitempty"`
	EndDate        *timestamppb.Timestamp `protobuf:"bytes,3,opt,name=end_date,json=endDate,proto3" json:"end_date,omitempty"`
	StopFactor     uint32                 `protobuf:"varint,4,opt,name=stop_factor,json=stopFactor,proto3" json:"stop_factor,omitempty"`
	ContractorType uint32                 `protobuf:"varint,5,opt,name=contractor_type,json=contractorType,proto3" json:"contractor_type,omitempty"`
	Type           uint32                 `protobuf:"varint,6,opt,name=type,proto3" json:"type,omitempty"`
	State          uint32                 `protobuf:"varint,7,opt,name=state,proto3" json:"state,omitempty"`
}

func (x *CreateSaleRequest_CreateSale) Reset() {
	*x = CreateSaleRequest_CreateSale{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_ui_v1_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateSaleRequest_CreateSale) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateSaleRequest_CreateSale) ProtoMessage() {}

func (x *CreateSaleRequest_CreateSale) ProtoReflect() protoreflect.Message {
	mi := &file_api_ui_v1_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateSaleRequest_CreateSale.ProtoReflect.Descriptor instead.
func (*CreateSaleRequest_CreateSale) Descriptor() ([]byte, []int) {
	return file_api_ui_v1_service_proto_rawDescGZIP(), []int{1, 0}
}

func (x *CreateSaleRequest_CreateSale) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CreateSaleRequest_CreateSale) GetStartDate() *timestamppb.Timestamp {
	if x != nil {
		return x.StartDate
	}
	return nil
}

func (x *CreateSaleRequest_CreateSale) GetEndDate() *timestamppb.Timestamp {
	if x != nil {
		return x.EndDate
	}
	return nil
}

func (x *CreateSaleRequest_CreateSale) GetStopFactor() uint32 {
	if x != nil {
		return x.StopFactor
	}
	return 0
}

func (x *CreateSaleRequest_CreateSale) GetContractorType() uint32 {
	if x != nil {
		return x.ContractorType
	}
	return 0
}

func (x *CreateSaleRequest_CreateSale) GetType() uint32 {
	if x != nil {
		return x.Type
	}
	return 0
}

func (x *CreateSaleRequest_CreateSale) GetState() uint32 {
	if x != nil {
		return x.State
	}
	return 0
}

type ImportSaleProductsRequest_ImportSaleProducts struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SaleID   uint32 `protobuf:"varint,1,opt,name=saleID,proto3" json:"saleID,omitempty"`
	Params   []byte `protobuf:"bytes,2,opt,name=params,proto3" json:"params,omitempty"`      // twirp не поддержиавет стримы :(
	RegionID uint32 `protobuf:"varint,3,opt,name=regionID,proto3" json:"regionID,omitempty"` // repeated uint32 regionsIDs = 3; подумать, как можно обойтись без перевода []uint32 протобафа в []uint
}

func (x *ImportSaleProductsRequest_ImportSaleProducts) Reset() {
	*x = ImportSaleProductsRequest_ImportSaleProducts{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_ui_v1_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ImportSaleProductsRequest_ImportSaleProducts) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ImportSaleProductsRequest_ImportSaleProducts) ProtoMessage() {}

func (x *ImportSaleProductsRequest_ImportSaleProducts) ProtoReflect() protoreflect.Message {
	mi := &file_api_ui_v1_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ImportSaleProductsRequest_ImportSaleProducts.ProtoReflect.Descriptor instead.
func (*ImportSaleProductsRequest_ImportSaleProducts) Descriptor() ([]byte, []int) {
	return file_api_ui_v1_service_proto_rawDescGZIP(), []int{2, 0}
}

func (x *ImportSaleProductsRequest_ImportSaleProducts) GetSaleID() uint32 {
	if x != nil {
		return x.SaleID
	}
	return 0
}

func (x *ImportSaleProductsRequest_ImportSaleProducts) GetParams() []byte {
	if x != nil {
		return x.Params
	}
	return nil
}

func (x *ImportSaleProductsRequest_ImportSaleProducts) GetRegionID() uint32 {
	if x != nil {
		return x.RegionID
	}
	return 0
}

var File_api_ui_v1_service_proto protoreflect.FileDescriptor

var file_api_ui_v1_service_proto_rawDesc = []byte{
	0x0a, 0x17, 0x61, 0x70, 0x69, 0x2f, 0x75, 0x69, 0x2f, 0x76, 0x31, 0x2f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x05, 0x75, 0x69, 0x2e, 0x76, 0x31,
	0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75,
	0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x1f,
	0x0a, 0x0b, 0x54, 0x65, 0x73, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x10, 0x0a,
	0x03, 0x6d, 0x73, 0x67, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6d, 0x73, 0x67, 0x22,
	0xe1, 0x02, 0x0a, 0x11, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x61, 0x6c, 0x65, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x43, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53,
	0x61, 0x6c, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x23, 0x2e, 0x75, 0x69, 0x2e, 0x76,
	0x31, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x61, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x61, 0x6c, 0x65, 0x52, 0x0a,
	0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x61, 0x6c, 0x65, 0x1a, 0x86, 0x02, 0x0a, 0x0a, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x61, 0x6c, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x39, 0x0a,
	0x0a, 0x73, 0x74, 0x61, 0x72, 0x74, 0x5f, 0x64, 0x61, 0x74, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x09, 0x73,
	0x74, 0x61, 0x72, 0x74, 0x44, 0x61, 0x74, 0x65, 0x12, 0x35, 0x0a, 0x08, 0x65, 0x6e, 0x64, 0x5f,
	0x64, 0x61, 0x74, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d,
	0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x07, 0x65, 0x6e, 0x64, 0x44, 0x61, 0x74, 0x65, 0x12,
	0x1f, 0x0a, 0x0b, 0x73, 0x74, 0x6f, 0x70, 0x5f, 0x66, 0x61, 0x63, 0x74, 0x6f, 0x72, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x0d, 0x52, 0x0a, 0x73, 0x74, 0x6f, 0x70, 0x46, 0x61, 0x63, 0x74, 0x6f, 0x72,
	0x12, 0x27, 0x0a, 0x0f, 0x63, 0x6f, 0x6e, 0x74, 0x72, 0x61, 0x63, 0x74, 0x6f, 0x72, 0x5f, 0x74,
	0x79, 0x70, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x0e, 0x63, 0x6f, 0x6e, 0x74, 0x72,
	0x61, 0x63, 0x74, 0x6f, 0x72, 0x54, 0x79, 0x70, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x74, 0x79, 0x70,
	0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a,
	0x05, 0x73, 0x74, 0x61, 0x74, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x05, 0x73, 0x74,
	0x61, 0x74, 0x65, 0x22, 0xe2, 0x01, 0x0a, 0x19, 0x49, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x53, 0x61,
	0x6c, 0x65, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x12, 0x63, 0x0a, 0x12, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x53, 0x61, 0x6c, 0x65, 0x50,
	0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x33, 0x2e,
	0x75, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x49, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x53, 0x61, 0x6c, 0x65,
	0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x2e,
	0x49, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x53, 0x61, 0x6c, 0x65, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x73, 0x52, 0x12, 0x69, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x53, 0x61, 0x6c, 0x65, 0x50, 0x72,
	0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x1a, 0x60, 0x0a, 0x12, 0x49, 0x6d, 0x70, 0x6f, 0x72, 0x74,
	0x53, 0x61, 0x6c, 0x65, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x12, 0x16, 0x0a, 0x06,
	0x73, 0x61, 0x6c, 0x65, 0x49, 0x44, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x06, 0x73, 0x61,
	0x6c, 0x65, 0x49, 0x44, 0x12, 0x16, 0x0a, 0x06, 0x70, 0x61, 0x72, 0x61, 0x6d, 0x73, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x0c, 0x52, 0x06, 0x70, 0x61, 0x72, 0x61, 0x6d, 0x73, 0x12, 0x1a, 0x0a, 0x08,
	0x72, 0x65, 0x67, 0x69, 0x6f, 0x6e, 0x49, 0x44, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x08,
	0x72, 0x65, 0x67, 0x69, 0x6f, 0x6e, 0x49, 0x44, 0x32, 0xa1, 0x01, 0x0a, 0x0b, 0x53, 0x61, 0x6c,
	0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x40, 0x0a, 0x0a, 0x43, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x53, 0x61, 0x6c, 0x65, 0x12, 0x18, 0x2e, 0x75, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x53, 0x61, 0x6c, 0x65, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12, 0x50, 0x0a, 0x12, 0x49, 0x6d,
	0x70, 0x6f, 0x72, 0x74, 0x53, 0x61, 0x6c, 0x65, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73,
	0x12, 0x20, 0x2e, 0x75, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x49, 0x6d, 0x70, 0x6f, 0x72, 0x74, 0x53,
	0x61, 0x6c, 0x65, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x42, 0x0d, 0x5a, 0x0b,
	0x2e, 0x2f, 0x3b, 0x73, 0x61, 0x6c, 0x65, 0x75, 0x69, 0x76, 0x31, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_api_ui_v1_service_proto_rawDescOnce sync.Once
	file_api_ui_v1_service_proto_rawDescData = file_api_ui_v1_service_proto_rawDesc
)

func file_api_ui_v1_service_proto_rawDescGZIP() []byte {
	file_api_ui_v1_service_proto_rawDescOnce.Do(func() {
		file_api_ui_v1_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_api_ui_v1_service_proto_rawDescData)
	})
	return file_api_ui_v1_service_proto_rawDescData
}

var file_api_ui_v1_service_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_api_ui_v1_service_proto_goTypes = []interface{}{
	(*TestRequest)(nil),                                  // 0: ui.v1.TestRequest
	(*CreateSaleRequest)(nil),                            // 1: ui.v1.CreateSaleRequest
	(*ImportSaleProductsRequest)(nil),                    // 2: ui.v1.ImportSaleProductsRequest
	(*CreateSaleRequest_CreateSale)(nil),                 // 3: ui.v1.CreateSaleRequest.CreateSale
	(*ImportSaleProductsRequest_ImportSaleProducts)(nil), // 4: ui.v1.ImportSaleProductsRequest.ImportSaleProducts
	(*timestamppb.Timestamp)(nil),                        // 5: google.protobuf.Timestamp
	(*emptypb.Empty)(nil),                                // 6: google.protobuf.Empty
}
var file_api_ui_v1_service_proto_depIdxs = []int32{
	3, // 0: ui.v1.CreateSaleRequest.createSale:type_name -> ui.v1.CreateSaleRequest.CreateSale
	4, // 1: ui.v1.ImportSaleProductsRequest.importSaleProducts:type_name -> ui.v1.ImportSaleProductsRequest.ImportSaleProducts
	5, // 2: ui.v1.CreateSaleRequest.CreateSale.start_date:type_name -> google.protobuf.Timestamp
	5, // 3: ui.v1.CreateSaleRequest.CreateSale.end_date:type_name -> google.protobuf.Timestamp
	1, // 4: ui.v1.SaleService.CreateSale:input_type -> ui.v1.CreateSaleRequest
	2, // 5: ui.v1.SaleService.ImportSaleProducts:input_type -> ui.v1.ImportSaleProductsRequest
	6, // 6: ui.v1.SaleService.CreateSale:output_type -> google.protobuf.Empty
	6, // 7: ui.v1.SaleService.ImportSaleProducts:output_type -> google.protobuf.Empty
	6, // [6:8] is the sub-list for method output_type
	4, // [4:6] is the sub-list for method input_type
	4, // [4:4] is the sub-list for extension type_name
	4, // [4:4] is the sub-list for extension extendee
	0, // [0:4] is the sub-list for field type_name
}

func init() { file_api_ui_v1_service_proto_init() }
func file_api_ui_v1_service_proto_init() {
	if File_api_ui_v1_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_api_ui_v1_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TestRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_ui_v1_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateSaleRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_ui_v1_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ImportSaleProductsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_ui_v1_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateSaleRequest_CreateSale); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_ui_v1_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ImportSaleProductsRequest_ImportSaleProducts); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_api_ui_v1_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_api_ui_v1_service_proto_goTypes,
		DependencyIndexes: file_api_ui_v1_service_proto_depIdxs,
		MessageInfos:      file_api_ui_v1_service_proto_msgTypes,
	}.Build()
	File_api_ui_v1_service_proto = out.File
	file_api_ui_v1_service_proto_rawDesc = nil
	file_api_ui_v1_service_proto_goTypes = nil
	file_api_ui_v1_service_proto_depIdxs = nil
}
