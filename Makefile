.PHONY: test
test:
	go test -race -count 100 ./...

.PHONY: protoc
protoc:
	rm -rf ./gen
	mkdir -p ./gen
	buf generate

.PHONY: lint
lint: golangci-lint buf-lint

.PHONY: golangci-lint
golangci-lint:
	golangci-lint run

.PHONY: buf-lint
buf-lint:
	buf lint

.PHONY: build-app
build-app:
	go build -o ./bin/app ./cmd/app.go
.PHONY: run-app
run-app:
	./bin/app

.PHONY: build-server
build-server:
	go build -o ./bin/server ./cmd/server.go

.PHONY: build-mars-consumer
build-creator-consumer:
	go build -o ./bin/creator_consumer ./cmd/creator.go
