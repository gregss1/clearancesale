FROM golang:1.17-alpine AS base-builder
RUN apk add --update --no-cache git ca-certificates

FROM base-builder AS builder
# устанавливаем рабочую директорию
WORKDIR /app/
# данные локального пользователя
ENV USER=appuser
ENV UID=10001
# добавляем пользователя
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"
# копируем файлы
COPY go.mod .
COPY go.sum .
# загружаем зависимости
RUN go mod download
# копируем все к себе
COPY . .
# собираем билд
RUN go build ./cmd/server/

FROM scratch AS prod
# устанавливаем рабочую директорию
WORKDIR /app/
# копируем билд
COPY --from=builder /app .
# копируем сертификаты
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# копируем пользоватлей
COPY --from=builder /etc/passwd /etc/passwd
# копируем группы
COPY --from=builder /etc/group /etc/group

USER appuser:appuser
# открываем порт
EXPOSE 8081
# запускаем сервер
CMD ["/app/server"]